﻿using ASEASSIGNMENT.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

namespace AseAssignmentTests.Tests
{
    [TestClass]
    public class PenColourTests
    {
        //Test to check correct colour is returned for pen command
        [TestMethod]
        public void PenColour_PenColourCommand_ReturnsColourRed()
        {
            //Arrange
            PenColour penColour = new PenColour();

            //Act
            var result = penColour.PenColourCommand("pen red");

            //Assert
            Assert.AreEqual(result, Color.Red);
        }

        //Test to check correct colour is returned for pen command
        [TestMethod]
        public void PenColour_PenColourCommand_ReturnsColourYellow()
        {
            //Arrange
            PenColour penColour = new PenColour();

            //Act
            var result = penColour.PenColourCommand("pen yellow");

            //Assert
            Assert.AreEqual(result, Color.Yellow);
        }

        //Test to check correct colour is returned for pen command
        [TestMethod]
        public void PenColour_PenColourCommand_ReturnsColourBlack()
        {
            //Arrange
            PenColour penColour = new PenColour();

            //Act
            var result = penColour.PenColourCommand("pen black");

            //Assert
            Assert.AreEqual(result, Color.Black);
        }

        //Test to check correct colour is returned for pen command
        [TestMethod]
        public void PenColour_PenColourCommand_ReturnsColourGreen()
        {
            //Arrange
            PenColour penColour = new PenColour();

            //Act
            var result = penColour.PenColourCommand("pen green");

            //Assert
            Assert.AreEqual(result, Color.Green);
        }
    }
}
