﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASEASSIGNMENT.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace AseAssignmentTests.Tests
{
    [TestClass]
    public class CommandHandlerTests
    {
        //unit test for part 2 returns correct variable names and values according to commands
        [TestMethod]
        public void CommandHandler_HandlerProcessesLoop_ReturnsCorrectVariableKeyAndValues()
        {
            //Arrange
            Bitmap bitmap = new Bitmap(640, 480);
            Graphics graphics;
            graphics = Graphics.FromImage(bitmap);
            TrackerObject trackerObj = new TrackerObject();
            Dictionary<string, int> variables = new Dictionary<string, int>();

            string[] multiBoxCommands = new string[]
            { 
                "var radius = 50",
                "loop for 10",
                "circle radius",
                "radius = radius + 10",
                "end for"
            };

            variables.Add("radius", 140);

            CommandHandler handler = new CommandHandler(graphics, new Pen(Color.Black),
                trackerObj, false);

            //Act
            var result = handler.HandleCommands(multiBoxCommands, new string[] { "" }, "run");

            //Assert
            Assert.AreEqual(result.ListOfVariables.Keys.First(), variables.Keys.First());
            Assert.AreEqual(result.ListOfVariables.Values.First(), variables.Values.First());
        }

        //unit test for part 2 check method to check if variable exists returns true
        [TestMethod]
        public void CommandHandler_HandlerChecksVariable_ReturnsTrueIfVariableExists()
        {
            //Arrange
            Bitmap bitmap = new Bitmap(640, 480);
            Graphics graphics;
            graphics = Graphics.FromImage(bitmap);
            TrackerObject trackerObj = new TrackerObject();
            Dictionary<string, int> variables = new Dictionary<string, int>();

            variables.Add("circle", 100);

            CommandHandler handler = new CommandHandler(graphics, new Pen(Color.Black),
                trackerObj, false);

            //Act
            var result = handler.CheckIfVariableExists("var circle = 100", variables, "var");

            //Assert
            Assert.AreEqual(result, true);
        }

        //unit test for part 2 check method to check if variable exists returns false
        [TestMethod]
        public void CommandHandler_HandlerChecksVariable_ReturnsFalseIfVariableDoesNotExist()
        {
            //Arrange
            Bitmap bitmap = new Bitmap(640, 480);
            Graphics graphics;
            graphics = Graphics.FromImage(bitmap);
            TrackerObject trackerObj = new TrackerObject();
            Dictionary<string, int> variables = new Dictionary<string, int>();

            CommandHandler handler = new CommandHandler(graphics, new Pen(Color.Black),
                trackerObj, false);

            //Act
            var result = handler.CheckIfVariableExists("var circle = 100", variables, "var");

            //Assert
            Assert.AreEqual(result, false);
        }

        //unit test for part 2 check method for correct operation type
        [DataTestMethod]
        [DataRow("radius = radius + 10", "+")]
        [DataRow("radius = radius / 10", "/")]
        [DataRow("radius = radius - 10", "-")]
        [TestMethod]
        public void CommandHandler_HandlerChecksOperator_ReturnsCorrectOperationType(string line, string operatorType)
        {
            //Arrange
            Bitmap bitmap = new Bitmap(640, 480);
            Graphics graphics;
            graphics = Graphics.FromImage(bitmap);
            TrackerObject trackerObj = new TrackerObject();
            Dictionary<string, int> variables = new Dictionary<string, int>();

            CommandHandler handler = new CommandHandler(graphics, new Pen(Color.Black),
                trackerObj, false);

            //Act
            var result = handler.CheckIfLineHasOperatorAndReturn(line);

            //Assert
            Assert.AreEqual(result, operatorType);
        }
    }
}
