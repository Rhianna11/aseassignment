﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASEASSIGNMENT.Classes;
using System.Collections.Generic;
using System.Linq;

namespace AseAssignmentTests.Tests
{
    [TestClass]
    public class CommandParserTests
    {
        //Test method to test correct type is returned (int array)
        [TestMethod]
        public void CommandParser_ParseCommandParametersToInt_ReturnsCorrectType()
        {
            //Arrange
            CommandParser parser = new CommandParser();

            //Act
            var result = parser.ParseCommandParametersToInt("moveto 100,100");

            //Assert
            Assert.IsInstanceOfType(result, typeof(int[]));
        }

        //Data test method to test correct length is returned in int array
        //Use data row to pass multiple parameters as tests
        [DataTestMethod]
        [DataRow("moveto 100,100")]
        [DataRow("drawto 200,200")]
        [DataRow("circle 10")]
        public void CommandParser_ParseCommandParametersToInt_ReturnsCorrectLength(string command)
        {
            //Arrange
            CommandParser parser = new CommandParser();

            //Act
            var result = parser.ParseCommandParametersToInt(command);

            //Assert
            Assert.IsInstanceOfType(result, typeof(int[]));
        }

        //Test method to test correct return type is of string
        [TestMethod]
        public void CommandParser_ParseCommandString_ReturnsCorrectType()
        {
            //Arrange
            CommandParser parser = new CommandParser();

            //Act
            var result = parser.ParseCommandString("moveto 100,100");

            //Assert
            Assert.IsInstanceOfType(result, typeof(string));
        }

        //Test method to test correct return type is list of key-value-pairs
        [TestMethod]
        public void CommandParser_ParseCommandOutputs_ReturnsCorrectType()
        {
            //Arrange
            CommandParser parser = new CommandParser();

            //Act
            var result = parser.ParseCommandOutputs("moveto 50,50", new List<KeyValuePair<string, int[]>>(), new Dictionary<string, int>());

            //Assert
            Assert.IsInstanceOfType(result, typeof(List<KeyValuePair<string, int[]>>));
        }

        //Test method to test correct return correct exception message
        [TestMethod]
        public void CommandParser_ParseCommandOutputs_ReturnsExceptionMessage()
        {
            //Arrange
            CommandParser parser = new CommandParser();
            string exception = "";

            //Act
            try
            {
                parser.ParseCommandOutputs("move to 100,100", new List<KeyValuePair<string, int[]>>(), new Dictionary<string, int>());
            }
            catch (FormatException e)
            {
                exception = e.Message;
            }

            //Assert
            Assert.AreEqual("Invalid parameters used : line 46", exception);
        }

        //Unit test for part 2 to parse variable
        [TestMethod]
        public void CommandParser_ParseVariable_ReturnsCorrectKeyAndValue()
        {
            //Arrange
            CommandParser parser = new CommandParser();
            Dictionary<string, int> variables = new Dictionary<string, int>();
            variables.Add("num", 50);

            //Act
            var result = parser.ParseVarValue("var num = 50", new Dictionary<string, int>());

            //Assert
            Assert.AreEqual(result.Keys.First(), variables.Keys.First());
            Assert.AreEqual(result.Values.First(), variables.Values.First());
        }
    }
}
