﻿namespace ASEASSIGNMENT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MultiCommandBox = new System.Windows.Forms.RichTextBox();
            this.CommandBox = new System.Windows.Forms.TextBox();
            this.DrawingArea = new System.Windows.Forms.PictureBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.SyntaxCheck = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DrawingArea)).BeginInit();
            this.SuspendLayout();
            // 
            // MultiCommandBox
            // 
            this.MultiCommandBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.MultiCommandBox.Location = new System.Drawing.Point(39, 13);
            this.MultiCommandBox.Name = "MultiCommandBox";
            this.MultiCommandBox.Size = new System.Drawing.Size(395, 456);
            this.MultiCommandBox.TabIndex = 0;
            this.MultiCommandBox.Text = "";
            // 
            // CommandBox
            // 
            this.CommandBox.Location = new System.Drawing.Point(39, 489);
            this.CommandBox.Name = "CommandBox";
            this.CommandBox.Size = new System.Drawing.Size(395, 20);
            this.CommandBox.TabIndex = 1;
            this.CommandBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandBox_KeyDown);
            // 
            // DrawingArea
            // 
            this.DrawingArea.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.DrawingArea.Location = new System.Drawing.Point(537, 13);
            this.DrawingArea.Name = "DrawingArea";
            this.DrawingArea.Size = new System.Drawing.Size(439, 456);
            this.DrawingArea.TabIndex = 2;
            this.DrawingArea.TabStop = false;
            this.DrawingArea.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawingArea_Paint);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(537, 489);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 3;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(630, 489);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 23);
            this.LoadButton.TabIndex = 4;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // SyntaxCheck
            // 
            this.SyntaxCheck.Location = new System.Drawing.Point(731, 489);
            this.SyntaxCheck.Name = "SyntaxCheck";
            this.SyntaxCheck.Size = new System.Drawing.Size(75, 23);
            this.SyntaxCheck.TabIndex = 5;
            this.SyntaxCheck.Text = "Syntax";
            this.SyntaxCheck.UseVisualStyleBackColor = true;
            this.SyntaxCheck.Click += new System.EventHandler(this.SyntaxCheck_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1029, 528);
            this.Controls.Add(this.SyntaxCheck);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.DrawingArea);
            this.Controls.Add(this.CommandBox);
            this.Controls.Add(this.MultiCommandBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.DrawingArea)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox MultiCommandBox;
        private System.Windows.Forms.TextBox CommandBox;
        private System.Windows.Forms.PictureBox DrawingArea;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button SyntaxCheck;
    }
}

