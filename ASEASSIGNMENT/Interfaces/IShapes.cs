﻿using System.Drawing;

namespace ASEASSIGNMENT.Interfaces
{
    //Interface for all shapes to inherit
    interface IShapes
    {
        void DrawShape(Graphics graphics, Pen pen, bool isFill);
    }
}
