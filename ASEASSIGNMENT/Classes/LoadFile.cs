﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace ASEASSIGNMENT.Classes
{
    class LoadFile
    {
        List<string> commandsFromFile = new List<string>();

        //Method to load text file
        public string[] LoadEvent()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            //Filter load dialog to only see txt files
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            try
            {
                //Get the path of specified file
                string fileContent;

                //Show dialog and read file selected
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        while ((fileContent = reader.ReadLine()) != null)
                        {
                            //Add file contents to List of strings
                            commandsFromFile.Add(fileContent);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            //Return populated list of strings
            return commandsFromFile.ToArray();
        }
    }
}
