﻿
using System.Collections.Generic;

namespace ASEASSIGNMENT.Classes
{
    //Class to track turtle position
    public class TrackerObject
    {
        public int TurtleXPos { get; set; }
        public int TurtleYPos { get; set; }

        public Dictionary<string, int> ListOfVariables = new Dictionary<string, int>();
        public List<string> ListOfExceptions = new List<string>();
    }
}
