﻿using ASEASSIGNMENT.Classes.Shapes;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace ASEASSIGNMENT.Classes
{
    public class CommandHandler
    {
        private List<KeyValuePair<string, int[]>> _codeOutputText = new List<KeyValuePair<string, int[]>>();
        private readonly Graphics _graphics;
        private readonly Pen _pen = new Pen(Color.Black);
        private TrackerObject _trackerObj = new TrackerObject();
        private bool _isFill, _variableExists, _loopFlag, _loopStartFlag, _endOfLoopFlag;
        private int _loopCount = 0, _iterations = 0, _loopBodyCount, iterationsInLoop = 0, originalBodyCount = 0;

        //CommandHandler constructor for passed parameters
        public CommandHandler(Graphics graphics, Pen pen, TrackerObject turtle, bool isFill)
        {
            _graphics = graphics;
            _pen = pen;
            _trackerObj.TurtleXPos = turtle.TurtleXPos;
            _trackerObj.TurtleYPos = turtle.TurtleYPos;
            _trackerObj.ListOfVariables = turtle.ListOfVariables;
            _isFill = isFill;
        }

        //Method to handle commands
        public TrackerObject HandleCommands(string[] multiCommandBox, string[] singleCommandBox, string command)
        {
            CommandParser parser = new CommandParser();

            if (multiCommandBox.Length > 0 && command.Equals("run"))
            {
                //add restart label so we can re-loop through multicommandbox when we want
                restart:

                foreach (var line in multiCommandBox)
                {
                    //if current line does not contains loop for, end for and loop flag is true
                    if (!line.Contains("loop for") && !line.Contains("end for") && _loopFlag)
                    {
                        //add loop iteration
                        iterationsInLoop++;
                    }

                    //check if we are in a loop
                    CheckIfLoop(line , multiCommandBox);

                    //if we are at the end of the loop then break out of the foreach
                    if (_endOfLoopFlag)
                    {
                        break;
                    }

                    //if current line does not contain loop for or end for commands then do things as normal
                    if (!line.Contains("loop for") && !line.Contains("end for"))
                    {
                         if (line.Contains("var"))
                         {
                             //check if attempted variable assignment exists
                             _variableExists = CheckIfVariableExists(line, _trackerObj.ListOfVariables, "var");

                            //Skip foreach item if _variableExists and _loopFlag is true 
                            if (_variableExists || _loopFlag)
                             {
                                 if (!_loopStartFlag)
                                 {
                                     _trackerObj.ListOfVariables = parser.SetVariableValue(line, _trackerObj.ListOfVariables, "");
                                 }

                                //Skip current item if we are at the start of the loop
                                continue;
                             }

                             //Continue to parse var value if variable doesnt already exist
                             if (_variableExists == false)
                             {
                                 _trackerObj.ListOfVariables = parser.ParseVarValue(line, _trackerObj.ListOfVariables);
                             }
                         }

                         else if (line.Contains("="))
                         {
                             _variableExists = CheckIfVariableExists(line, _trackerObj.ListOfVariables, "=");

                            //if variable doesnt exist but no 'var' declaration used then throw exception
                            if (_variableExists == false)
                            {
                                _trackerObj.ListOfExceptions.Add(line.Split(' ')[0] + " variable has not been declared using 'var' : line 77\n");
                            }
                            else
                            {
                                //check if current line has an operator in it then return it
                                var operatorType = CheckIfLineHasOperatorAndReturn(line);

                                //if variable exists and '=' used then assign the variable and pass relevant params
                                if (_variableExists == true)
                                {
                                    _trackerObj.ListOfVariables = parser.SetVariableValue(line, _trackerObj.ListOfVariables, operatorType);
                                }
                            }
                         }

                         //if non of the above criteria is met then parse command as normal
                         else if (multiCommandBox.Length > 0 && command.Equals("run"))
                         {
                             _codeOutputText = parser.ParseCommandOutputs(line, _codeOutputText, _trackerObj.ListOfVariables);
                         }         
                    }

                    //if loop flag is true and current line contains end for command
                    //then set loop flag to false and restart foreach to re-check if we are still in a loop
                    if (_loopFlag == true && line.Contains("end for"))
                    {
                        _loopFlag = false;
                        goto restart;
                    }
                }
            }

            if (singleCommandBox.Length > 0)
            {
                foreach (var line in singleCommandBox)
                {
                    if (command.Contains("var"))
                    {
                        _variableExists = CheckIfVariableExists(line, _trackerObj.ListOfVariables, "var");

                        if (_variableExists == true)
                        {
                            _trackerObj.ListOfExceptions.Add(line.Split(' ')[1] + " already exists: line 80 \n");
                        }
                        else
                        {
                            _trackerObj.ListOfVariables = parser.ParseVarValue(line, _trackerObj.ListOfVariables);
                        }
                    }

                    else if (line.Contains("="))
                    {
                        _variableExists = CheckIfVariableExists(line, _trackerObj.ListOfVariables, "=");
                        var operatorType = CheckIfLineHasOperatorAndReturn(line);

                        if (_variableExists == false)
                        {
                            _trackerObj.ListOfExceptions.Add(line.Split(' ')[0] + " variable has not been declared using 'var' : line 132\n");
                        }
                        else if (_variableExists == true)
                        {
                           _trackerObj.ListOfVariables = parser.SetVariableValue(line, _trackerObj.ListOfVariables, operatorType);
                        }
                    }

                    else if (singleCommandBox.Length > 0 && !command.Equals("run"))
                    {
                        //Else parse single line commands
                        _codeOutputText = parser.ParseCommandOutputs(line, _codeOutputText, _trackerObj.ListOfVariables);
                    }
                }
            }

            if (_codeOutputText.Count > 0 && !_loopFlag)
            {
                //loop through populated key-value-pairs of parsed commands
                foreach (KeyValuePair<string, int[]> commands in _codeOutputText)
                {
                    //Check current key for a match
                    switch (commands.Key)
                    {
                        case "drawto":
                            //Check parameters have been populated
                            if (commands.Value.Length <= 1)
                            {
                                _trackerObj.ListOfExceptions.Add("Invalid parameters for command " + commands.Key + "\n");
                            }
                            else
                            {
                                //Initialize new Line and pass relevant parameters to its constructor
                                new Line(_graphics, _pen, _trackerObj.TurtleXPos, _trackerObj.TurtleYPos, commands.Value[0], commands.Value[1], _isFill);

                                //Set turtle position based on parameters
                                _trackerObj.TurtleXPos = commands.Value[0];
                                _trackerObj.TurtleYPos = commands.Value[1];
                            }
                            break;
                        case "moveto":
                            //Check parameters have been populated
                            if (commands.Value.Length <= 1)
                            {
                                _trackerObj.ListOfExceptions.Add("Invalid parameters for command " + commands.Key + "\n");
                            }
                            else
                            {
                                _trackerObj.TurtleXPos = commands.Value[0];
                                _trackerObj.TurtleYPos = commands.Value[1];
                            }
                            break;
                        case "clear":
                            //Clear graphics and set background to it's original colour
                            _graphics.Clear(SystemColors.AppWorkspace);
                            break;
                        case "reset":
                            _trackerObj.TurtleXPos = 0;
                            _trackerObj.TurtleYPos = 0;
                            break;
                        case "circle":
                            //Check parameters have been populated
                            if (commands.Value.Length > 1)
                            {
                                _trackerObj.ListOfExceptions.Add("Invalid parameters for command " + commands.Key + ": line 199\n");
                            }
                            else
                            {
                                //Initialize new Circle and pass relevant parameters to its constructor
                                new Circle(_graphics, _pen, _trackerObj.TurtleXPos, _trackerObj.TurtleYPos, commands.Value[0], _isFill);
                            }
                            break;
                        case "rectangle":
                            //Check parameters have been populated
                            if (commands.Value.Length <= 1)
                            {
                                _trackerObj.ListOfExceptions.Add("Invalid parameters for command " + commands.Key + ": line 211\n");
                            }
                            else
                            {
                                //Initialize new Rect and pass relevant parameters to its constructor
                                new Rect(_graphics, _pen, _trackerObj.TurtleXPos, _trackerObj.TurtleYPos, commands.Value[0], commands.Value[1], _isFill);
                            }
                            break;
                        case "triangle":
                            //Check parameters have been populated
                            if (commands.Value.Length <= 1)
                            {
                                _trackerObj.ListOfExceptions.Add("Invalid parameters for command " + commands.Key + ": line 221\n");
                            }
                            else
                            {
                                //Initialize new Triangle and pass relevant parameters to its constructor
                                new Triangle(_graphics, _pen, _trackerObj.TurtleXPos, _trackerObj.TurtleYPos, commands.Value[0], commands.Value[1], _isFill);
                            }
                            break;
                        default:
                            _trackerObj.ListOfExceptions.Add("No command found for " + command + ": line 234 \n");
                            break;
                    }
                }
            }

            return _trackerObj;
        }

        public bool CheckIfVariableExists(string varToBeDefined, Dictionary<string, int> listOfVariables, string operation)
        {
            switch (operation)
            {
                case "var":
                    foreach (KeyValuePair<string, int> variable in listOfVariables)
                    {
                        if (varToBeDefined.Split(' ')[1].Equals(variable.Key))
                        {
                            return true;
                        }
                    }
                    break;

                case "=":
                    foreach (KeyValuePair<string, int> variable in listOfVariables)
                    {
                        if (varToBeDefined.Split(' ')[0].Equals(variable.Key))
                        {
                            return true;
                        }
                    }
                    break;          
            }
          
            return false;
        }

        public string CheckIfLineHasOperatorAndReturn(string line)
        {
            var operatorArr = new[] { "/", "+", "-" };

            for (var i = 0; i < operatorArr.Length; i++)
            {
                if (line.Contains(operatorArr[i]))
                {
                    return operatorArr[i];
                }
            }

            //returns empty string if no operator found
            return "";
        }

        public void CheckIfLoop(string line, string[] commands)
        {
            //if current line contains "loop" and loop flag is false
            if (line.Contains("loop") && _loopFlag == false)
            {
                //find where the index of the loop command is within our multi text box and the end for command is
                //so we can count the loop body
                int startLoopIndex = Array.FindIndex(commands, row => row.Contains("loop for"));
                int endLoopIndex = Array.FindIndex(commands, row => row.Contains("end for"));

                //If the loop body hasnt been hit yet then set its value
                if (_loopBodyCount == 0)
                {
                    _loopBodyCount = endLoopIndex - startLoopIndex - 1;
                    originalBodyCount = _loopBodyCount;
                    _loopStartFlag = true;
                }

                //grab the number from the loop command so we know how many loops we are doing
                _loopCount = Convert.ToInt32(line.Split(' ')[0 + 2]);

                //set loop flag to true
                _loopFlag = true;               
            }

            //if we are in a loop and the number of iteration equals the loop body count to increment 
            //both the body count and the iterations
            if (_loopFlag == true && iterationsInLoop == _loopBodyCount)
            {
                _loopBodyCount += originalBodyCount;
                _iterations++;
            }

            //if number of iterations matches the amount of loops then reset all loop values
            if (_loopCount == _iterations && _iterations != 0)
            {
                _loopCount = 0;
                _iterations = 0;
                _loopBodyCount = 0;
                originalBodyCount = 0;
                iterationsInLoop = 0;
                _loopFlag = false;
                _endOfLoopFlag = true;
                _loopStartFlag = false;
            }
        }
    }
}
