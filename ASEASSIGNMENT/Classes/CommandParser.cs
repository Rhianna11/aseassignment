﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ASEASSIGNMENT.Classes
{
    public class CommandParser
    {
        //Method to return List of key-value-pairs populated with commands
        public List<KeyValuePair<string, int[]>> ParseCommandOutputs(string command, List<KeyValuePair<string, int[]>> commandList, Dictionary<string, int> variableList)
        {
            try
            {
                List<int> variableValueList = new List<int>();

                //Loop through command string array
                //Check command contains fill or pen to prevent invalid parsing
                if (!command.Contains("fill") && !command.Contains("pen"))
                {
                    foreach (KeyValuePair<string, int> variable in variableList)
                    {
                        //Regular expression to check for string match within the command with variable name(key)
                        if (Regex.IsMatch(command, @"\b" + variable.Key + @"\b"))
                        {
                             variableValueList.Add(variable.Value);
                             commandList.Add(new KeyValuePair<string, int[]>(ParseCommandString(command), variableValueList.ToArray()));
                        }                      
                    }

                    //Parse command as normal for clear and reset to avoid parse errors
                    if (command.Equals("clear") || command.Equals("reset"))
                    {
                        commandList.Add(new KeyValuePair<string, int[]>(ParseCommandString(command), ParseCommandParametersToInt(command)));
                    }

                    //check if current command param can be parsed to int and if it can then 'ParseCommandParametersToInt' method can be used
                    else if (int.TryParse(command.Split(',', ' ')[1], out _))
                    {
                        //If commands valid then parse command/parameters into key-value-pair list from methods
                        commandList.Add(new KeyValuePair<string, int[]>(ParseCommandString(command), ParseCommandParametersToInt(command)));
                    }

                    else if (!int.TryParse(command.Split(',', ' ')[1], out _) && commandList.Count < 1)
                    {
                        throw new FormatException("Invalid parameters used : line 46");
                    }
                }
            }
            catch (Exception e)
            {
                throw new FormatException(e.Message);
            }

            return commandList;
        }

        //Method to parse parameters
        public int[] ParseCommandParametersToInt(string command)
        {
            //Split array contents based on commas
            string[] commandArray = command.Split(',', ' ');
            List<int> parameterArray = new List<int>();

            try
            {
                for (int i = 1; i < commandArray.Length; i++)
                {
                    //Parse paramter integers from string array and store into integer array
                    parameterArray.Add(Convert.ToInt32(commandArray[i]));
                }
            }
            catch
            {
                throw new FormatException("Invalid parameters used for command on line 46 " + command);
            }

            return parameterArray.ToArray();
        }

        //Method to parse command
        public string ParseCommandString(string command)
        {
            //Split based on space
            string[] commandArray = command.Split(' ');

            //Return expected first command index from array
            return commandArray[0];
        }

        public Dictionary<string, int> ParseVarValue(string command, Dictionary<string, int> variableList)
        {
            //Split based on space
            string[] commandArray = command.Split(' ');
            int variableValue = 0;

            try
            {
                //find the index of'=' on current command so we can handle the value
                int indexOfVar = Array.FindIndex(commandArray, m => m == "=");

                //retrieve currently called variable's value
                variableValue = Convert.ToInt32(commandArray[indexOfVar + 1]);

                //add variable name and value to the variable list
                variableList.Add(commandArray[1], variableValue);
            }
            catch
            {
                throw new FormatException("Invalid variable assignment on line 77 " + command);
            }

            return variableList;
        }

        public Dictionary<string, int> SetVariableValue(string command, Dictionary<string, int> variableList, string operatorType)
        {
            //Split based on space
            string[] commandArray = command.Split(' ');
            int indexOfVar;

            try
            {
                //if an operator is found in current command then we must do the math
                if (!string.IsNullOrEmpty(operatorType))
                {
                    //index of current commands operator
                    indexOfVar = Array.FindIndex(commandArray, m => m == operatorType);

                    //get the value for the variable that's been called
                    int variableValue = variableList.FirstOrDefault(x => x.Key == commandArray[0]).Value;

                    //pass the value to be handles mathemtically along with the operator type and current variable value
                    int newVarValue = HandleOperatorValue(operatorType, Convert.ToInt32(commandArray[indexOfVar + 1]), variableValue);

                    //set variable new value based off the math used
                    variableList[commandArray[0]] = newVarValue;
                }
                else
                {
                    //else no operators found so assign value as normal
                    indexOfVar = Array.FindIndex(commandArray, m => m == "=");

                    if (commandArray[0] == "var")
                    {
                        variableList[commandArray[1]] = Convert.ToInt32(commandArray[indexOfVar + 1]);
                    }
                    else
                    {
                        variableList[commandArray[0]] = Convert.ToInt32(commandArray[indexOfVar + 1]);
                    }
                }
            }
            catch
            {
                throw new FormatException("Invalid variable assignment on line 93" + command);
            }

            return variableList;
        }

        public int HandleOperatorValue(string operatorType, int valueToOperate, int VariableValue)
        {
            //check which operator is used and do the math then return new variable value total
            switch (operatorType)
            {
                case "+":
                    VariableValue = valueToOperate + VariableValue;
                    break;

                case "-":
                    VariableValue = valueToOperate - VariableValue;
                    break;

                case "/":
                    VariableValue = VariableValue / valueToOperate;
                    break;
            }

            return VariableValue;
        }
    }
}
