﻿using System.Drawing;

namespace ASEASSIGNMENT.Classes
{
    public class PenColour
    {
        //Method to set pen colour based on 'pen' command
        public Color PenColourCommand(string command)
        {
            //Return match colour or set black if not found
            switch (command)
            {
                case "pen red":
                    return Color.Red;
                case "pen green":
                    return Color.Green;
                case "pen yellow":
                    return Color.Yellow;
                case "pen black":
                    return Color.Black;
                default:
                    return Color.Black;
            }
        }
    }
}
