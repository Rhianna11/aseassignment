﻿using ASEASSIGNMENT.Interfaces;
using System.Drawing;

namespace ASEASSIGNMENT.Classes.Shapes
{
    class Circle : IShapes
    {
        private int _toXPos, _toYPos;
        private readonly int _radius;
        private readonly Graphics _graphics;
        private readonly Pen _pen;
        private readonly bool _isFill;

        //Circle constructor for passed parameters
        public Circle(Graphics graphics, Pen pen, int toXPos, int toYPos, int radius, bool isFill)
        {
            _graphics = graphics;
            _radius = radius;
            _pen = pen;
            _toXPos = toXPos;
            _toYPos = toYPos;
            _isFill = isFill;

            DrawShape(_graphics, _pen, _isFill);
        }

        public void DrawShape(Graphics graphics, Pen pen, bool isFill)
        {
            if (isFill)
            {
                graphics.FillEllipse(new SolidBrush(_pen.Color), _toXPos - _radius, _toYPos - _radius,
                    _radius + _radius, _radius + _radius);
            }
            else
            {
                graphics.DrawEllipse(pen, _toXPos - _radius, _toYPos - _radius,
                    _radius + _radius, _radius + _radius);
            }
        }
    }
}
