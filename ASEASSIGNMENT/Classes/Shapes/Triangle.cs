﻿using ASEASSIGNMENT.Interfaces;
using System.Drawing;

namespace ASEASSIGNMENT.Classes.Shapes
{
    class Triangle : IShapes
    {
        private int _toXPos, _toYPos, _xPos, _yPos;
        private readonly Graphics _graphics;
        private readonly Pen _pen;
        private readonly bool _isFill;

        //Triangle constructor for passed parameters
        public Triangle(Graphics graphics, Pen pen, int toXPos, int toYPos, int xPos, int yPos, bool isFill)
        {
            _graphics = graphics;
            _pen = pen;
            _toXPos = toXPos;
            _toYPos = toYPos;
            _xPos = xPos;
            _yPos = yPos;
            _isFill = isFill;

            DrawShape(_graphics, _pen, _isFill);
        }

        public void DrawShape(Graphics graphics, Pen pen, bool isFill)
        {
            Point point1 = new Point(_toXPos, _toYPos);
            Point point2 = new Point(_xPos, _yPos);
            Point point3 = new Point(_yPos, _toXPos);

            Point[] curvePoints =
            {
                 point1,
                 point2,
                 point3,
            };

            if (isFill)
            {
                graphics.FillPolygon(new SolidBrush(_pen.Color), curvePoints);
            }
            else
            {
                graphics.DrawPolygon(pen, curvePoints);
            }
        }
    }
}
