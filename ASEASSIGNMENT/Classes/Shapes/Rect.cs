﻿using ASEASSIGNMENT.Interfaces;
using System.Drawing;

namespace ASEASSIGNMENT.Classes.Shapes
{
    class Rect : IShapes
    {
        private int _toXPos, _toYPos;
        private readonly int _width, _height;
        private readonly Graphics _graphics;
        private readonly Pen _pen;
        private readonly bool _isFill;

        //Rect constructor for passed parameters
        public Rect(Graphics graphics, Pen pen, int toXPos, int toYPos, int width, int height, bool isFill)
        {
            _graphics = graphics;
            _width = width;
            _height = height;
            _pen = pen;
            _toXPos = toXPos;
            _toYPos = toXPos;
            _isFill = isFill;

            DrawShape(_graphics, _pen, _isFill);
        }

        public void DrawShape(Graphics graphics, Pen pen, bool isFill)
        {
            if (isFill)
            {
                graphics.FillRectangle(new SolidBrush(_pen.Color), new Rectangle(_toXPos, _toYPos, _width, _height));
            }
            else
            {
                graphics.DrawRectangle(pen, new Rectangle(_toXPos, _toYPos, _width, _height));
            }
        }
    }
}
