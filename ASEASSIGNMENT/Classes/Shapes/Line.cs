﻿using ASEASSIGNMENT.Interfaces;
using System.Drawing;

namespace ASEASSIGNMENT.Classes.Shapes
{
    class Line : IShapes
    {
        private int _xPos, _yPos, _toXPos, _toYPos;
        private readonly Graphics _graphics;
        private readonly Pen _pen;
        private readonly bool _isFill;

        //Line constructor for passed parameters
        public Line(Graphics graphics, Pen pen, int toXPos, int toYPos, int xPos, int yPos, bool isFill)
        {
            _graphics = graphics;
            _pen = pen;
            _xPos = xPos;
            _yPos = yPos;
            _toXPos = toXPos;
            _toYPos = toYPos;
            _isFill = isFill;

            DrawShape(_graphics, _pen, _isFill);
        }

        public void DrawShape(Graphics graphics, Pen pen, bool isFill)
        {
            graphics.DrawLine(pen, _xPos, _yPos, _toXPos, _toYPos);
        }
    }
}
