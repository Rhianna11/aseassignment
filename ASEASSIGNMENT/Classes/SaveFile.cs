﻿using System;
using System.IO;
using System.Windows.Forms;

namespace ASEASSIGNMENT.Classes
{
    class SaveFile
    {
        //Method to Save multi text box contents to file
        public void SaveEvent(string[] multiBoxCommands)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            //Only store file as txt or doc file
            saveFileDialog.Filter = "Text Files|*.txt|Documents|*.doc";
            saveFileDialog.ShowDialog();
            try
            {
                using (StreamWriter file = new StreamWriter(saveFileDialog.FileName))
                {
                    foreach (string command in multiBoxCommands)
                    {
                        //Write to file
                        file.WriteLine(command);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
