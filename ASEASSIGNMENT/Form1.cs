﻿using ASEASSIGNMENT.Classes;
using ASEASSIGNMENT.Classes.Shapes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEASSIGNMENT
{
    public partial class Form1 : Form
    {
        private readonly Graphics _graphics;
        private readonly Bitmap _bitmap = new Bitmap(640, 480);
        private readonly Pen _pen = new Pen(Color.Black);
        private TrackerObject _trackerObj = new TrackerObject();
        private PenColour _penColour = new PenColour();
        private bool _isFill = false;

        public Form1()
        {
            InitializeComponent();
            _graphics = Graphics.FromImage(_bitmap);

            //Initialize turtle position
            _trackerObj.TurtleXPos = 0;
            _trackerObj.TurtleYPos = 0;
        }

        private void DrawingArea_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(_bitmap, 0, 0);
            //Draw turtle pointer
            e.Graphics.FillRectangle(Brushes.Green, _trackerObj.TurtleXPos, _trackerObj.TurtleYPos, 5, 5);
        }

        private void CommandBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Trim and lower case of inputted command
                string command = CommandBox.Text.Trim().ToLower();

                //Check if command is fill and set bool appropriately
                if (command.Equals("fill on") || MultiCommandBox.Text.Contains("fill on"))
                {
                    _isFill = true;
                }
                else if (command.Equals("fill off") || MultiCommandBox.Text.Contains("fill off"))
                {
                    _isFill = false;
                }

                //Check for pen command
                if (command.Contains("pen"))
                {
                    //Set pen colour based on called methods switch case and command
                    _pen.Color = _penColour.PenColourCommand(command);
                }

                CommandHandler commandHandler = new CommandHandler(_graphics, _pen, _trackerObj, _isFill);

                try
                {
                    //Handle/begin parsing command if its not fill or pen
                    if (command != null && !command.Contains("fill") && !command.Contains("pen"))
                    {
                        TrackerObject trackerObj = commandHandler.HandleCommands(MultiCommandBox.Lines, CommandBox.Lines, command);
                        _trackerObj = trackerObj;
                    }

                    if (_trackerObj.ListOfExceptions.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();

                        foreach (var exception in _trackerObj.ListOfExceptions)
                        {
                            sb.Append(exception);
                        }

                        MessageBox.Show(sb.ToString());
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }

                CommandBox.Text = "";
                Refresh();
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //When save is clicked only call save event method if text not null
                if (MultiCommandBox.Text != "")
                {
                    //Pass multi text box contents as array of strings
                    SaveFile saveFile = new SaveFile();
                    saveFile.SaveEvent(MultiCommandBox.Lines);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadFile loadFile = new LoadFile();
                string[] commandsFromFile = loadFile.LoadEvent();

                //Load from txt file and display into multi text box applying new lines when appropriate
                MultiCommandBox.Text = string.Join("\n", commandsFromFile);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void SyntaxCheck_Click(object sender, EventArgs e)
        {
            CommandHandler commandHandler = new CommandHandler(_graphics, _pen, _trackerObj, _isFill);
            //Trim and lower case of inputted command
            string command = CommandBox.Text.Trim().ToLower();

            TrackerObject trackerObj = commandHandler.HandleCommands(MultiCommandBox.Lines, CommandBox.Lines, "run");

            if (trackerObj.ListOfExceptions.Count > 0)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var exception in trackerObj.ListOfExceptions)
                {
                    sb.Append(exception);
                }

                MessageBox.Show(sb.ToString());
            }
        }
    }
}
